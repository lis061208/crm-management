# CRM-Management-System---Spring-boot #

This project is developed in Spring boot using Java 8. Database used to store records is PostgreSQL. Maven is used for building jar file and docker is used to create containers. Swagger is used to maintain the API documentation. This version of code allows someone to get the order the details, create order with multiple items, create/update bulk order and delete order entry from the system.
CRM-Management is the complete solution to your inventory management problems.
Our cloud-based software is built to handle your purchasing, sales, and restocking needs on any device.

## Business Use Cases ##

* create purchase orders and email them to vendors
* set reorder points to prevent running out of stock
* manage stock across one or more locations
* create sales orders from any device
* scan to pick, receive, transfer, or ship
* assemble products from bill of materials (BOM)
* generate barcodes and labels
* sell online through B2B Showroom and CRM-Management Pay
* pull ecommerce orders from Shopify, Amazon, and more
* create your own integrations with CRM-Management's API

CRM-Management is used most often used for:

* wholesale
* distribution
* manufacturing
* ecommerce
* asset tracking
* field service management
 

Development life cycle :

### Phase 1 : Customer Management ###
* User Management
* Preferences Management
* Organization Management
* Group Management

### Phase 2 : Inventory Management ###
* Alerts/Notifications
* Barcoding / RFID
* Forecasting
* Inventory Optimization
* Kitting
* Manufacturing Inventory Management
* Mobile Access
* Multi-Channel Management
* Product Identification
* Reorder Management
* Reporting/Analytics
* Supplier Management
* Warehouse Management
* Retail Inventory Management

### Phase 3 : Manufacturing ###
* Accounting Integration
* Purchase Order Management
* Shipping Management
* Maintenance Management
* Quality Management
* Quotes/Estimates
* Reporting/Analytics
* Safety Management
 
### Phase 4 : Order Management ###
* Back Order Management
* Catalog Management
* Inventory Management
* Order Entry
* Order Fulfillment
* Order Tracking
* Returns Management
* Shipping Management
* Recurring Orders
* Special Order Management
 
### Phase 5 : Warehouse Management ###
* Barcoding / RFID
* Category Customization
* Channel Management
* Demand Planning
* Inventory Management
* Location Control
* Order Management
* Purchasing
* Quality Control